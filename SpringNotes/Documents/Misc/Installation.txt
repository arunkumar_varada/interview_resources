Download Spring software from "www.springsource.org/download/"

SPRING_HOME=D:\SPRING\softwares\spring-framework-3.0.3.RELEASE

Spring Jars:
%SPRING_HOME%\dist\*.jar

Spring API Docs:
%SPRING_HOME%\docs\javadoc-api\index.html

Spring Reference manual:
%SPRING_HOME%\docs\spring-framework-reference\pdf\spring-framework-reference.pdf

Spring Namespaces:
------------------
1) %SPRING_HOME%\projects\org.springframework.beans\src\main\resources\org\springframework\beans\factory\xml\spring-beans-3.0.xsd (Spring's beans namespace)
2) %SPRING_HOME%\projects\org.springframework.aop\src\main\resources\org\springframework\aop\config\spring-aop-3.0.xsd (Spring's AOP namespace)
3) %SPRING_HOME%\projects\org.springframework.context\src\main\resources\org\springframework\context\config\spring-context-3.0.xsd (Spring's context namespace)
4) %SPRING_HOME%\projects\org.springframework.transaction\src\main\resources\org\springframework\transaction\config\spring-tx-3.0.xsd (Spring's Transaction namespace)
5) %SPRING_HOME%\projects\org.springframework.web.servlet\src\main\resources\org\springframework\web\servlet\config\spring-mvc-3.0.xsd (Spring's MVC namespace)