				Directory Structure
				===================
Download hibernate framework from "www.hibernate.org"
HIB_HOME=D:\HibernateWorkshop\softwares\hibernate-distribution-3.5.6-Final

Hibernate API Documentation:
%HIB_HOME%\documentation\javadocs\index.html

Hibernate Reference Manual:
%HIB_HOME%\documentation\manual\en-US\pdf\hibernate_reference.pdf

Compiletime dependent jar files:
%HIB_HOME%\hibernate3.jar

Runtime dependent jar files:
%HIB_HOME%\lib\
	\bytecode\*.jar
	\jpa\*.jar
	\optional\*.jar
	\required\*.jar

Framework source code:
%HIB_HOME%\project\core\src\main\java

Resources:
%HIB_HOME%\project\core\src\main\resources\org\hibernate\*.dtd


Hibernate Properties file:
%HIB_HOME%\project\etc\hibernate.properties










